package com.alibaba;

import com.netflix.loadbalancer.BestAvailableRule;
import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RoundRobinRule;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@MapperScan(value = "com.alibaba.dao")
@EnableEurekaClient
@SpringBootApplication
public class Consumer80 {

    public static void main(String[] args) {
        SpringApplication.run(Consumer80.class, args);
    }

    // Ribbon = 客户端负载均衡 + RestTemplate远程调用
    // 使用RestTemplate进行远程调用其他微服务
    @LoadBalanced // 开启负载均衡
    @Bean
    public RestTemplate getRestTemplate() {
        return new RestTemplate();
    }

    // Ribbon负载均衡策略
    @Bean
    public IRule getIrule() {
        /*
         . 更改负载均衡的策略，换成其它类对象：
         . RandomRule        : 随机策略 随机选择Server
         . RoundRobinRule    : 轮询策略 按照顺序选择Server（Ribbon 默认策略）
         . RetryRule         : 重试策略 在一个配置时间段内，当选择 server 不成功，则一直尝试选择一个可用的Server
         . BestAvailableRule : 最低并发 策略 逐个考察 server，如果 server 断路器打开，则忽略，再选择其中并发链接最低的Server
         */
        return new RoundRobinRule();
    }

}
