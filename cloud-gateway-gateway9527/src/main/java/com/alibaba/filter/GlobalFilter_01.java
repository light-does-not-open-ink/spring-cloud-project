package com.alibaba.filter;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/*
 * 这是一个 "自定义全局" 过滤器：
 *           PS:全局过滤器不需要在application.yml文件中设置
 *           1.实现 GlobalFilter, Ordered两个接口
 *           2.重写方法：public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain):
 *           3.重写方法：public int getOrder(); 返回该全局过滤器的优先级
 */
@Component
public class GlobalFilter_01 implements GlobalFilter, Ordered {

    private static final Logger LOGGER = LoggerFactory.getLogger(GlobalFilter_01.class);

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        // 获取请求参数的Map集合
        MultiValueMap<String, String> queryParams = exchange.getRequest().getQueryParams();

        // 获取请求参数token值
        String token = queryParams.getFirst("token");
        if (StringUtils.isEmpty(token)) {

            ServerHttpResponse response = exchange.getResponse();

            // 结束本次请求，返回响应报文
            LOGGER.error("全局过滤器测试：" + response.toString());
        }
        System.out.println("获取到请求参数为：" + token);
        // 放行
        return chain.filter(exchange);
    }

    // 设置全局过滤器的优先级，0为最高优先级
    @Override
    public int getOrder() {
        return 0;
    }
}
