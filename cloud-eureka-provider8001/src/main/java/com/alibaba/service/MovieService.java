package com.alibaba.service;

import com.alibaba.pojo.Movie;

/**
 * @author LGQ
 */
public interface MovieService {

    Movie findMovieById(Integer id);
}
