package com.alibaba.remoteCall;

import com.alibaba.pojo.Movie;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

/**
 * @author LGQ
 * <p>
 * 基于OpenFeign的远程调用接口，用于远程调用其他微服务
 * <p>
 * Feign中已经自动集成了Ribbon负载均衡，因此不需要自己定义RestTemplate进行负载均衡的配置。
 */
@FeignClient("CLOUD-EUREKA-PROVIDER8001") //被调用服务在注册中心的注册名字
public interface MovieFeignClient {

    /* 被远程调用的方法：（"/movie/info"）
      @RequestMapping("/info")
      public Movie findMovieById(Integer id) {
          LOGGER.warn("访问了port:" + port);
          return movieService.findMovieById(id);
      }  */
    // 在使用OpenFeign进行远程调用时:
    //         1.接口方法的返回值和参数列表必须与被调用的方法一致，
    //           但是方法名可以不一致但是推荐方法名也一致
    //         2.被调用方法只能使用@RequestMapping or @PostMapping
    //         3 如果需要使用@GetMapping，则需使用Restful风格的url以及@PathVariable("xxx")
    //         4.简单类型参数使用@RequestParam("xxx")
    //         5.json参数使用@RequestBody
    //         6.路径参数使用@PathVariable("xxx")
    @RequestMapping("/movie/info")
    Map<String,Object> findMovieById(@RequestParam("id") Integer id);

}