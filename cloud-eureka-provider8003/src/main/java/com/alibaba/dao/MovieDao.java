package com.alibaba.dao;

import com.alibaba.pojo.Movie;
import org.apache.ibatis.annotations.Select;

/**
 * @author LGQ
 */
public interface MovieDao {

    @Select("select movie_id movieId,movie_name movieName,movie_price moviePrice " +
            "from t_movie where movie_id = #{id}")
    Movie findMovieById(Integer id);

}
