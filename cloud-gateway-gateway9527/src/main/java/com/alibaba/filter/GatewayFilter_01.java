package com.alibaba.filter;


import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;

/*
 * GateWay中三大组件：
 *           1、路由(交换机)
 *           2、断言：predicates
 *           3、filters
 * <p>
 * 这是一个 "自定义" 过滤器：
 *           1.继承 AbstractGatewayFilterFactory
 *           2.重写抽象方法：public GatewayFilter apply(Object config);
 *           3.重写：public String name(){} 方法返回自定义过滤器的名称
 */
@Component
public class GatewayFilter_01 extends AbstractGatewayFilterFactory {

    private static final Logger LOGGER = LoggerFactory.getLogger(GatewayFilter_01.class);

    /**
     * 重写apply(Object config)方法，自定义规则拦截请求
     */
    @Override
    public GatewayFilter apply(Object config) {
        return (exchange, chain) -> {
            MultiValueMap<String, String> queryParams = exchange.getRequest().getQueryParams();
            String name = queryParams.getFirst("name");
            if (StringUtils.isEmpty(name)) {
                LOGGER.error("请求没有携带一个参数名为name的参数");
                LOGGER.error("这是一个自定义过滤器测试类，如果没有携带该参数，请求会触发"
                        + this.getClass().getName() + "过滤器，但请求依旧会通过");
            } else {
                LOGGER.info("name参数值：" + name);
            }
            return chain.filter(exchange);
        };
    }

    @Override
    public String name() {
        return getThisClassName();
    }

    private String getThisClassName() {
        // String packageName = com.alibaba.filter.本类名
        String packageName = this.getClass().getName();
        // 返回类名
        return packageName.substring(packageName.lastIndexOf(".") + 1);
    }
}
