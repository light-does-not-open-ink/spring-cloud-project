package com.alibaba.controller;

import com.alibaba.pojo.Movie;
import com.alibaba.pojo.User;
import com.alibaba.remoteCall.MovieFeignClient;
import com.alibaba.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/user")
public class UserController {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

    @Value("${server.port}")
    private String port;

    @Autowired
    private UserService userService;

    @Autowired
    private MovieFeignClient movieFeignClient;

    @GetMapping("/info")
    public Map<String, Object> findUserById(Integer id) {
        Map<String, Object> map = new HashMap<>();
        User user = userService.findUserById(id);
        map.put("user", user);
        LOGGER.warn("访问了port:" + this.port);

        /**
         * 使用加入OpenFeign后的自定义远程调用接口:com.alibaba.remoteCall.MovieFeignClient,
         * 调用与被调用同名同参数同返回值的接口方法，以实现远程调用
         * 与Ribbon负载均衡+RestTemplate的远程调用方式相对比：
         * 1、避免手动拼接url以及参数，使用更加方便
         */
        Map<String, Object> movieMap = movieFeignClient.findMovieById(100);
        Object movie = movieMap.get("movie");
        Object port = movieMap.get("port");
        map.put("moviePort",port);
        map.put("movie", movie);
        map.put("userPort-OpenFeign",this.port);
        return map;
    }


}
