package com.alibaba.service;


import com.alibaba.pojo.User;

public interface UserService {

    User findUserById(Integer id);

}
