package com.alibaba.dao;

import com.alibaba.pojo.User;
import org.apache.ibatis.annotations.Select;

public interface UserDao {

    @Select("select user_id userId,user_name userName,age,sex " +
            "from t_user where user_id = #{id}")
    User findUserById(Integer id);

}
