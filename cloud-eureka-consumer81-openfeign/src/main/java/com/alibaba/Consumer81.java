package com.alibaba;

import feign.Logger;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;

@EnableFeignClients //开启 Feign 支持,实现基于接口的远程 调用
@MapperScan(value = "com.alibaba.dao")
@SpringBootApplication
public class Consumer81 {

    public static void main(String[] args) {
        SpringApplication.run(Consumer81.class, args);
    }

    /**
     * 查看 OpenFeign 的远程调用日志:
     *         1、在配置类中配置 Feign 的 Logger.Level
     *         2、在 yml 中设置 feign 接口的日志级别：debug
     *         3、重启OpenFeign客户端服务
     */
    @Bean
    public Logger.Level level() {
        /** NONE：默认的，不显示任何日志
         * BASIC：仅记录请求方法、RUL、响应状态码及执行时间
         * HEADERS：除了BASIC中定义的信息之外，还有请求和响应的头信息
         * FULL：除了HEADERS中定义的信息之外，还有请求和响应的正文及元数据
         */
        return Logger.Level.FULL;
    }

}
