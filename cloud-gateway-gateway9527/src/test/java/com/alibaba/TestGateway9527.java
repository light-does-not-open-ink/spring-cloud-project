package com.alibaba;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class TestGateway9527 {

    @Test
    void contextLoads() {
        String string = this.getClass().getName(); // com.alibaba.TestGateway9527
        int i = string.lastIndexOf(".");
        String substring = string.substring(i+1);

        System.out.println(substring);
    }

}
