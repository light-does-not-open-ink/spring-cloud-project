package com.alibaba.service;

import com.alibaba.dao.MovieDao;
import com.alibaba.pojo.Movie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author LGQ
 */
@Service
public class MovieServiceImpl implements MovieService {

    @Autowired
    private MovieDao movieDao;

    @Override
    public Movie findMovieById(Integer id) {
        return movieDao.findMovieById(id);
    }
}
