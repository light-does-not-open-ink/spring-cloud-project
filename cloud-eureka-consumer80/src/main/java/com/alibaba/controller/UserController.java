package com.alibaba.controller;

import com.alibaba.pojo.User;
import com.alibaba.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@RestController
@RequestMapping("/user")
public class UserController {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

    @Value("${server.port}")
    private String port;

    @Autowired
    private UserService userService;

    @Autowired
    private RestTemplate restTemplate;

    @GetMapping("/info")
    public Map<String, Object> findUserById(Integer id) {
        Map<String, Object> map = new HashMap<>();
        User user = userService.findUserById(id);
        map.put("user", user);
        LOGGER.warn("访问了port:" + this.port);
        /*
         . 使用RestTemplate远程调用其他微服务:
         . url:     http://被调用的微服务在注册中心的名称/movie/info?id=50，
         . Class<T> responseType：被调用的微服务方法的返回值的字节码对象。
         */
        Map movieMap = Objects.requireNonNull(restTemplate.getForEntity
                ("http://CLOUD-EUREKA-PROVIDER8001/movie/info?id=50", Map.class).getBody());
        Object movie = movieMap.get("movie");
        Object port = movieMap.get("port");
        map.put("movie", movie);
        map.put("moviePort", port);
        map.put("userPort-Ribbon",this.port);
        return map;
    }


}
