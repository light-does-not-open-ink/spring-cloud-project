package com.alibaba;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @author LGQ
 */
@MapperScan(value = "com.alibaba.dao")
@EnableEurekaClient
@SpringBootApplication
public class Provider8002 {

    public static void main(String[] args) {
        SpringApplication.run(Provider8002.class, args);
    }

}
