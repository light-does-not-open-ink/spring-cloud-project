package com.alibaba.controller;

import com.alibaba.pojo.Movie;
import com.alibaba.service.MovieService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * @author LGQ
 */
@RestController
@RequestMapping("/movie")
public class MovieController {

    private static final Logger LOGGER = LoggerFactory.getLogger(MovieController.class);

    @Value("${server.port}")
    private String port;

    @Autowired
    private MovieService movieService;

    @RequestMapping("/info")
    public Map<String, Object> findMovieById(Integer id) {
        Map<String, Object> map = new HashMap<>();
        map.put("port",port);
        LOGGER.warn("访问了port:" + port);
        Movie movie= movieService.findMovieById(id);
        map.put("movie",movie);
        return map;
    }

}
