package com.alibaba.pojo;

import java.io.Serializable;

/**
 * @author LGQ
 */
public class Movie implements Serializable {
    private Integer movieId;
    private String movieName;
    private Double moviePrice;

    public Movie() {
    }

    public Movie(Integer movieId, String movieName, Double moviePrice) {
        this.movieId = movieId;
        this.movieName = movieName;
        this.moviePrice = moviePrice;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "movieId=" + movieId +
                ", movieName='" + movieName + '\'' +
                ", moviePrice=" + moviePrice +
                '}';
    }

    public Integer getMovieId() {
        return movieId;
    }

    public void setMovieId(Integer movieId) {
        this.movieId = movieId;
    }

    public String getMovieName() {
        return movieName;
    }

    public void setMovieName(String movieName) {
        this.movieName = movieName;
    }

    public Double getMoviePrice() {
        return moviePrice;
    }

    public void setMoviePrice(Double moviePrice) {
        this.moviePrice = moviePrice;
    }
}
